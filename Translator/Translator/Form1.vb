﻿Public Class Form1
    Dim Translations(4) As String
    Private Sub ExitBtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitBtn.Click
        Me.Close()
    End Sub

    Private Sub ListBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EnglishPhrasesListBox.SelectedIndexChanged
        Dim selected As Integer = EnglishPhrasesListBox.SelectedIndex
        Me.TranslationLabel.Text = Translations.ElementAt(selected)
    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Translations(0) = New String("Buenos días")
        Translations(1) = New String("Gracias")
        Translations(2) = New String("Adiós")
        Translations(3) = New String("Dinero")
    End Sub
End Class
