﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.EnglishPhrasesListBox = New System.Windows.Forms.ListBox()
        Me.TranslationLabel = New System.Windows.Forms.Label()
        Me.ExitBtn = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'EnglishPhrasesListBox
        '
        Me.EnglishPhrasesListBox.FormattingEnabled = True
        Me.EnglishPhrasesListBox.Items.AddRange(New Object() {"Good morning", "Thank you", "Goodbye", "Money"})
        Me.EnglishPhrasesListBox.Location = New System.Drawing.Point(12, 12)
        Me.EnglishPhrasesListBox.Name = "EnglishPhrasesListBox"
        Me.EnglishPhrasesListBox.Size = New System.Drawing.Size(120, 212)
        Me.EnglishPhrasesListBox.TabIndex = 0
        '
        'TranslationLabel
        '
        Me.TranslationLabel.AutoSize = True
        Me.TranslationLabel.Location = New System.Drawing.Point(147, 12)
        Me.TranslationLabel.Name = "TranslationLabel"
        Me.TranslationLabel.Size = New System.Drawing.Size(0, 13)
        Me.TranslationLabel.TabIndex = 1
        '
        'ExitBtn
        '
        Me.ExitBtn.Location = New System.Drawing.Point(12, 230)
        Me.ExitBtn.Name = "ExitBtn"
        Me.ExitBtn.Size = New System.Drawing.Size(260, 23)
        Me.ExitBtn.TabIndex = 2
        Me.ExitBtn.Text = "Exit"
        Me.ExitBtn.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 261)
        Me.Controls.Add(Me.ExitBtn)
        Me.Controls.Add(Me.TranslationLabel)
        Me.Controls.Add(Me.EnglishPhrasesListBox)
        Me.Name = "Form1"
        Me.Text = "English to Spanish Translator"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents EnglishPhrasesListBox As System.Windows.Forms.ListBox
    Friend WithEvents TranslationLabel As System.Windows.Forms.Label
    Friend WithEvents ExitBtn As System.Windows.Forms.Button

End Class
