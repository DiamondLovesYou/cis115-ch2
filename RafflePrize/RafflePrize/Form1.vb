﻿Public Class Form1
    Dim PrizeSelected As Integer = 0

    Private Sub UpdateVisablePicture()
        If PrizeSelected = 1 Then
            GasGrillPictureBox.Visible = True
        Else
            GasGrillPictureBox.Visible = False
        End If
        If PrizeSelected = 2 Then
            TVPictureBox.Visible = True
        Else
            TVPictureBox.Visible = False
        End If
        If PrizeSelected = 3 Then
            LaptopPictureBox.Visible = True
        Else
            LaptopPictureBox.Visible = False
        End If
    End Sub

    Private Sub GasGrillRadioBtn_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GasGrillRadioBtn.CheckedChanged
        PrizeSelected = 1
        SelectPrizeBtn.Enabled = True
        UpdateVisablePicture()
    End Sub

    Private Sub TVRadioBtn_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TVRadioBtn.CheckedChanged
        PrizeSelected = 2
        SelectPrizeBtn.Enabled = True
        UpdateVisablePicture()
    End Sub

    Private Sub LaptopRadioBtn_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LaptopRadioBtn.CheckedChanged
        PrizeSelected = 3
        SelectPrizeBtn.Enabled = True
        UpdateVisablePicture()
    End Sub

    Private Sub ExitBtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitBtn.Click
        Me.Close()
    End Sub

    Private Sub SelectPrizeBtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SelectPrizeBtn.Click
        PrizeSelectedMsg.Visible = True
        ExitBtn.Visible = True
    End Sub
End Class
