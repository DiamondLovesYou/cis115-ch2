﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GasGrillRadioBtn = New System.Windows.Forms.RadioButton()
        Me.TVRadioBtn = New System.Windows.Forms.RadioButton()
        Me.LaptopRadioBtn = New System.Windows.Forms.RadioButton()
        Me.SelectPrizeBtn = New System.Windows.Forms.Button()
        Me.GasGrillPictureBox = New System.Windows.Forms.PictureBox()
        Me.TVPictureBox = New System.Windows.Forms.PictureBox()
        Me.LaptopPictureBox = New System.Windows.Forms.PictureBox()
        Me.PrizeSelectedMsg = New System.Windows.Forms.Label()
        Me.ExitBtn = New System.Windows.Forms.Button()
        CType(Me.GasGrillPictureBox, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TVPictureBox, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LaptopPictureBox, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GasGrillRadioBtn
        '
        Me.GasGrillRadioBtn.AutoSize = True
        Me.GasGrillRadioBtn.Location = New System.Drawing.Point(12, 242)
        Me.GasGrillRadioBtn.Name = "GasGrillRadioBtn"
        Me.GasGrillRadioBtn.Size = New System.Drawing.Size(64, 17)
        Me.GasGrillRadioBtn.TabIndex = 0
        Me.GasGrillRadioBtn.TabStop = True
        Me.GasGrillRadioBtn.Text = "Gas Grill"
        Me.GasGrillRadioBtn.UseVisualStyleBackColor = True
        '
        'TVRadioBtn
        '
        Me.TVRadioBtn.AutoSize = True
        Me.TVRadioBtn.Location = New System.Drawing.Point(82, 242)
        Me.TVRadioBtn.Name = "TVRadioBtn"
        Me.TVRadioBtn.Size = New System.Drawing.Size(94, 17)
        Me.TVRadioBtn.TabIndex = 1
        Me.TVRadioBtn.TabStop = True
        Me.TVRadioBtn.Text = "Flat-screen TV"
        Me.TVRadioBtn.UseVisualStyleBackColor = True
        '
        'LaptopRadioBtn
        '
        Me.LaptopRadioBtn.AutoSize = True
        Me.LaptopRadioBtn.Location = New System.Drawing.Point(182, 242)
        Me.LaptopRadioBtn.Name = "LaptopRadioBtn"
        Me.LaptopRadioBtn.Size = New System.Drawing.Size(58, 17)
        Me.LaptopRadioBtn.TabIndex = 2
        Me.LaptopRadioBtn.TabStop = True
        Me.LaptopRadioBtn.Text = "Laptop"
        Me.LaptopRadioBtn.UseVisualStyleBackColor = True
        '
        'SelectPrizeBtn
        '
        Me.SelectPrizeBtn.Enabled = False
        Me.SelectPrizeBtn.Location = New System.Drawing.Point(82, 266)
        Me.SelectPrizeBtn.Name = "SelectPrizeBtn"
        Me.SelectPrizeBtn.Size = New System.Drawing.Size(75, 23)
        Me.SelectPrizeBtn.TabIndex = 3
        Me.SelectPrizeBtn.Text = "Select Prize"
        Me.SelectPrizeBtn.UseVisualStyleBackColor = True
        '
        'GasGrillPictureBox
        '
        Me.GasGrillPictureBox.Image = Global.RafflePrize.My.Resources.Resources.Grill
        Me.GasGrillPictureBox.Location = New System.Drawing.Point(12, 12)
        Me.GasGrillPictureBox.Name = "GasGrillPictureBox"
        Me.GasGrillPictureBox.Size = New System.Drawing.Size(223, 206)
        Me.GasGrillPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.GasGrillPictureBox.TabIndex = 4
        Me.GasGrillPictureBox.TabStop = False
        Me.GasGrillPictureBox.Visible = False
        '
        'TVPictureBox
        '
        Me.TVPictureBox.Image = Global.RafflePrize.My.Resources.Resources.TV
        Me.TVPictureBox.Location = New System.Drawing.Point(12, 12)
        Me.TVPictureBox.Name = "TVPictureBox"
        Me.TVPictureBox.Size = New System.Drawing.Size(223, 206)
        Me.TVPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.TVPictureBox.TabIndex = 5
        Me.TVPictureBox.TabStop = False
        Me.TVPictureBox.Visible = False
        '
        'LaptopPictureBox
        '
        Me.LaptopPictureBox.Image = Global.RafflePrize.My.Resources.Resources.Laptop
        Me.LaptopPictureBox.Location = New System.Drawing.Point(12, 12)
        Me.LaptopPictureBox.Name = "LaptopPictureBox"
        Me.LaptopPictureBox.Size = New System.Drawing.Size(223, 206)
        Me.LaptopPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.LaptopPictureBox.TabIndex = 6
        Me.LaptopPictureBox.TabStop = False
        Me.LaptopPictureBox.Visible = False
        '
        'PrizeSelectedMsg
        '
        Me.PrizeSelectedMsg.AutoSize = True
        Me.PrizeSelectedMsg.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PrizeSelectedMsg.Location = New System.Drawing.Point(79, 292)
        Me.PrizeSelectedMsg.Name = "PrizeSelectedMsg"
        Me.PrizeSelectedMsg.Size = New System.Drawing.Size(84, 13)
        Me.PrizeSelectedMsg.TabIndex = 7
        Me.PrizeSelectedMsg.Text = "Prize chosen!"
        Me.PrizeSelectedMsg.Visible = False
        '
        'ExitBtn
        '
        Me.ExitBtn.Location = New System.Drawing.Point(82, 308)
        Me.ExitBtn.Name = "ExitBtn"
        Me.ExitBtn.Size = New System.Drawing.Size(75, 23)
        Me.ExitBtn.TabIndex = 8
        Me.ExitBtn.Text = "Exit"
        Me.ExitBtn.UseVisualStyleBackColor = True
        Me.ExitBtn.Visible = False
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(247, 342)
        Me.Controls.Add(Me.ExitBtn)
        Me.Controls.Add(Me.PrizeSelectedMsg)
        Me.Controls.Add(Me.LaptopPictureBox)
        Me.Controls.Add(Me.TVPictureBox)
        Me.Controls.Add(Me.GasGrillPictureBox)
        Me.Controls.Add(Me.SelectPrizeBtn)
        Me.Controls.Add(Me.LaptopRadioBtn)
        Me.Controls.Add(Me.TVRadioBtn)
        Me.Controls.Add(Me.GasGrillRadioBtn)
        Me.Name = "Form1"
        Me.Text = "Form1"
        CType(Me.GasGrillPictureBox, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TVPictureBox, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LaptopPictureBox, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GasGrillRadioBtn As System.Windows.Forms.RadioButton
    Friend WithEvents TVRadioBtn As System.Windows.Forms.RadioButton
    Friend WithEvents LaptopRadioBtn As System.Windows.Forms.RadioButton
    Friend WithEvents SelectPrizeBtn As System.Windows.Forms.Button
    Friend WithEvents GasGrillPictureBox As System.Windows.Forms.PictureBox
    Friend WithEvents TVPictureBox As System.Windows.Forms.PictureBox
    Friend WithEvents LaptopPictureBox As System.Windows.Forms.PictureBox
    Friend WithEvents PrizeSelectedMsg As System.Windows.Forms.Label
    Friend WithEvents ExitBtn As System.Windows.Forms.Button

End Class
