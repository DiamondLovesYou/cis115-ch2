﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.BankPicture = New System.Windows.Forms.PictureBox()
        Me.DaysOpenLabel = New System.Windows.Forms.Label()
        Me.HoursOpenLabel = New System.Windows.Forms.Label()
        Me.ExitBtn = New System.Windows.Forms.Button()
        Me.ViewHoursBtn = New System.Windows.Forms.Button()
        CType(Me.BankPicture, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(335, 52)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(289, 25)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "First Corner National Bank"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(427, 77)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(103, 20)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "FDIC insured"
        '
        'BankPicture
        '
        Me.BankPicture.Image = Global.Bank.My.Resources.Resources.BankBuilding
        Me.BankPicture.Location = New System.Drawing.Point(55, 12)
        Me.BankPicture.Name = "BankPicture"
        Me.BankPicture.Size = New System.Drawing.Size(182, 272)
        Me.BankPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.BankPicture.TabIndex = 2
        Me.BankPicture.TabStop = False
        '
        'DaysOpenLabel
        '
        Me.DaysOpenLabel.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DaysOpenLabel.AutoSize = True
        Me.DaysOpenLabel.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DaysOpenLabel.Location = New System.Drawing.Point(190, 301)
        Me.DaysOpenLabel.Name = "DaysOpenLabel"
        Me.DaysOpenLabel.Size = New System.Drawing.Size(134, 54)
        Me.DaysOpenLabel.TabIndex = 3
        Me.DaysOpenLabel.Text = "Monday - Thursday" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Friday" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Saturday"
        Me.DaysOpenLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.DaysOpenLabel.Visible = False
        '
        'HoursOpenLabel
        '
        Me.HoursOpenLabel.AutoSize = True
        Me.HoursOpenLabel.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.HoursOpenLabel.Location = New System.Drawing.Point(330, 301)
        Me.HoursOpenLabel.Name = "HoursOpenLabel"
        Me.HoursOpenLabel.Size = New System.Drawing.Size(161, 54)
        Me.HoursOpenLabel.TabIndex = 4
        Me.HoursOpenLabel.Text = "9:00 a.m. to 5:00 p.m." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "9:00 a.m. to 8:00 p.m." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "9:00 a.m. to 1:00 p.m."
        Me.HoursOpenLabel.Visible = False
        '
        'ExitBtn
        '
        Me.ExitBtn.Location = New System.Drawing.Point(243, 467)
        Me.ExitBtn.Name = "ExitBtn"
        Me.ExitBtn.Size = New System.Drawing.Size(144, 30)
        Me.ExitBtn.TabIndex = 5
        Me.ExitBtn.Text = "Exit"
        Me.ExitBtn.UseVisualStyleBackColor = True
        '
        'ViewHoursBtn
        '
        Me.ViewHoursBtn.Location = New System.Drawing.Point(243, 431)
        Me.ViewHoursBtn.Name = "ViewHoursBtn"
        Me.ViewHoursBtn.Size = New System.Drawing.Size(144, 30)
        Me.ViewHoursBtn.TabIndex = 6
        Me.ViewHoursBtn.Text = "View Banking Hours"
        Me.ViewHoursBtn.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(671, 509)
        Me.Controls.Add(Me.ViewHoursBtn)
        Me.Controls.Add(Me.ExitBtn)
        Me.Controls.Add(Me.HoursOpenLabel)
        Me.Controls.Add(Me.DaysOpenLabel)
        Me.Controls.Add(Me.BankPicture)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Name = "Form1"
        Me.Text = "Welcome"
        CType(Me.BankPicture, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents BankPicture As System.Windows.Forms.PictureBox
    Friend WithEvents DaysOpenLabel As System.Windows.Forms.Label
    Friend WithEvents HoursOpenLabel As System.Windows.Forms.Label
    Friend WithEvents ExitBtn As System.Windows.Forms.Button
    Friend WithEvents ViewHoursBtn As System.Windows.Forms.Button

End Class
